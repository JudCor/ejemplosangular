import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-saludador',
  templateUrl: './saludador.component.html',
  styleUrls: ['./saludador.component.css']
})
export class SaludadorComponent implements OnInit {
destinos: DestinoViaje[];
  constructor() { }
    
  ngOnInit(): void {
      this.destinos= [];
  }

  guardar(nombre:string, url:string):boolean {
    this.destinos.push(new DestinoViaje(nombre,url));
    console.log(this.destinos);
    return false;
  }
}
